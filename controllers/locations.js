var model = require('../models');
var Location = model.location;

exports.listAll = function (req, res, next) {
  Location.find(function (err, locations) {
    if (err) {
      next(err);
    }

    res.json(locations);
  });
};

exports.listByCountry = function(req, res, next){
  var param = req.params.country;
  console.log(param);
  Location.find({'country': param}, function(err, locations){
    if(err){
      next(err);
    }
    res.json(locations);
  });
};

exports.listByCounty = function(req, res, next){
  var param = req.params.country;
  var paramTwo = req.params.county;
  Location.find({'country': param, 'county': paramTwo}, function(err, locations){
    if(err){
      next(err);
    }
    res.json(locations);
  });
};

exports.addLocation = function (req, res, next) {

  var location = new Location({
    name: req.body.name,
    country: req.body.country,
    county: req.body.county,
    lat: req.body.lat,
    long: req.body.long,
    directions: req.body.directions,
  });

  console.log(location);

  location.save(function (err) {
    if (err) {
      next(err);
    }

    console.log("Location added!");
    res.send(location);
  });
};

exports.findLocation = function (req, res, next) {
  var locationName = req.params.name;
  Location.findOne({ name: locationName }, function (err, location) {
    if (err) {
      return next(err);
    }

    if (!location) {
      var error = {
        error: 'No location with that name',
      };
      return res.json(error);
    }

    return res.json(location);
  });

};
