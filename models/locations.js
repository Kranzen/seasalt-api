// Dependencies
var mongoose = require('mongoose');

// Schema
var location = new mongoose.Schema({
  name: {
    type: String,
    require: true,
  },
  country: {
    type: String,
    require: true,
  },
  provins: {
    type: String,
    require: true,
  },
  lat: {
    type: Number,
    require: true,
  },
  long: {
    type: Number,
    require: true,
  },
  directions: {
    type: Array,
  },
});

// Export model

//module.exports = locationSchema;
module.exports = mongoose.model('location', location);
