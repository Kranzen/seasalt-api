// Dependencies
var express = require('express');
var router = express.Router();
var controller = require('../controllers');
var locationCtrl = controller.locations;

router.get('/locations', locationCtrl.listAll);

router.get('/locations/:country', locationCtrl.listByCountry);

router.get('/locations/:country/:county', locationCtrl.listByCounty);

router.post('/locations', locationCtrl.addLocation);

router.get('*', function (req, res) {
  res.send('Sorry dude, page not found!');
});

module.exports = router;
